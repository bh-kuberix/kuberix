import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import CommonTable from '../../component/table/CommonTable';
import CommonTableColumn from '../../component/table/CommonTableColumn';
import CommonTableRow from '../../component/table/CommonTableRow';
import * as common from "../../common";

const PostList = props => {
  const [ posts, setPosts ] = useState([]);

  useEffect(() => {
    axios.get(common.getServerUrl() + '/v1/posts')
      .then((result)=>{
        console.log(result.data);
        setPosts(result.data.posts);
      })
      .catch((error)=>{ console.error(error) })
  }, [ ])

  return (
    <CommonTable headersName={['글번호', '제목', '수정일시']}>
      {
        posts ? posts.map((post, index) => {
          return (
            <CommonTableRow key={index}>
              <CommonTableColumn>{ post.postId }</CommonTableColumn>
              <CommonTableColumn>
                <Link to={`/postView/${post.postId}`}>{ post.title }</Link>
              </CommonTableColumn>
              <CommonTableColumn>{ post.updatedDateTime }</CommonTableColumn>
            </CommonTableRow>
          )
        }) : 'no data'
      }
    </CommonTable>
  )
}

export default PostList;