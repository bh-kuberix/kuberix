import React, { useEffect, useState } from 'react';
import './Post.css';
import axios from 'axios';
import * as common from "../../common";

const PostView = ({ history, location, match }) => {
  const [ post, setPost ] = useState({});

  const postId = match.params.postId;

  useEffect(() => {
    axios.get(common.getServerUrl() + '/v1/posts/' + postId)
      .then((result)=>{
        console.log(result.data);
        setPost(result.data.post);
      })
      .catch((error)=>{ console.error(error) })
  }, [ postId ]);

  return (
    <>
      <h2 align="center">게시글 상세정보</h2>

      <div className="post-view-wrapper">
        {
          post ? (
            <>
              <div className="post-view-row">
                <label>게시글 번호</label>
                <label>{ post.postId }</label>
              </div>
              <div className="post-view-row">
                <label>제목</label>
                <label>{ post.title }</label>
              </div>
              <div className="post-view-row">
                <label>작성일시</label>
                <label>{ post.createdDateTime }</label>
              </div>
              <div className="post-view-row">
                <label>수정일시</label>
                <label>{ post.updatedDateTime }</label>
              </div>
              <div className="post-view-row">
                <label>내용</label>
                <div>
                  {
                    post.content
                  }
                </div>
              </div>
            </>
          ) : '해당 게시글을 찾을 수 없습니다.'
        }
        <button className="post-view-go-list-btn" onClick={() => history.goBack()}>목록으로 돌아가기</button>
      </div>
    </>
  )
}

export default PostView;