export function getServerUrl() {
	return process.env.REACT_APP_SERVER_URL === undefined ? "http://localhost:8080" : process.env.REACT_APP_SERVER_URL; 
}