package com.kuberix.dev_guide.post.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.kuberix.dev_guide.common.util.MessageUtil;
import com.kuberix.dev_guide.post.domain.Post;
import com.kuberix.dev_guide.post.mapper.PostMapper;

//@RunWith(MockitoJUnitRunner.class)
@RunWith(PowerMockRunner.class)
public class PostServiceTest {
	@Mock
	PostMapper postMapper;
	@Mock
	MessageUtil messageUtil;

	@Spy
	@InjectMocks
	PostService postService = PowerMockito.spy(new PostService());

	@Test
	public void listPost_postId_list() {
		List<Post> postMocks = new ArrayList<>();
		Post postMock = new Post();
		postMock.setPostId(1);
		postMocks.add(postMock);
		when(postMapper.listPosts(any(Post.class))).thenReturn(postMocks);

		List<Post> posts = postService.listPosts(null);

		assertTrue(!posts.isEmpty());
		assertEquals(1, posts.get(0).getPostId().intValue());
	}

	@Test
	public void createPost_post_newId() {
		Post postParam = new Post();
		postParam.setPostId(1);
		postParam.setTitle("title");
		postParam.setContent("content");
		when(postService.readPost(1)).thenReturn(postParam);

		when(messageUtil.getErrorMessage(anyVararg())).thenReturn("error");
		when(postMapper.createPost(any(Post.class))).thenReturn(1);
		when(postMapper.readCreatedPostId()).thenReturn(1);

		Integer postId = postService.createPost(postParam);

		assertNotNull(postId);
	}

	@Test
	public void readPost_postId_post() {
		Post postMock = new Post();
		postMock.setPostId(1);
		when(postMapper.readPost(any(Post.class))).thenReturn(postMock);

		Post post = postService.readPost(1);

		assertEquals(1, post.getPostId().intValue());
	}

	@Test
	public void updatePost_post_calling() {
		Post postMock = new Post();
		postMock.setPostId(1);
		when(postService.readPost(1)).thenReturn(postMock);

		when(postMapper.updatePost(any(Post.class))).thenReturn(1);

		Post post = new Post();
		postService.updatePost(1, post);

		verify(postMapper).updatePost(any(Post.class));
	}

	@Test
	public void deletePost_postId_calling() {
		Post postMock = new Post();
		postMock.setPostId(1);
		when(postService.readPost(1)).thenReturn(postMock);

		when(postMapper.deletePost(any(Post.class))).thenReturn(1);

		postService.deletePost(1);

		verify(postMapper).deletePost(any(Post.class));
	}
}
