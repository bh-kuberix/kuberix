package com.kuberix.dev_guide.post.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.kuberix.dev_guide.common.exception.NotFoundException;
import com.kuberix.dev_guide.common.util.MessageUtil;
import com.kuberix.dev_guide.post.domain.Post;
import com.kuberix.dev_guide.post.domain.PostWrapper;
import com.kuberix.dev_guide.post.service.PostService;

@RunWith(PowerMockRunner.class)
public class PostControllerTest {
	@Mock
	PostService postService;
	@Mock
	MessageUtil messageUtil;

	@Spy
	@InjectMocks
	PostController postController = PowerMockito.spy(new PostController());

	@Test
	public void readPost_postId_post() {
		Post postMock = new Post();
		postMock.setPostId(1);
		when(postService.readPost(1)).thenReturn(postMock);

		PostWrapper postWrapper = postController.readPost(1);

		assertEquals(1, postWrapper.getPost().getPostId().intValue());
	}

	@Test(expected = NotFoundException.class)
	public void readPost_badId_exception() {
		when(postService.readPost(1)).thenReturn(null);

		postController.readPost(1);
	}
}
