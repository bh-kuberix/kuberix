package com.kuberix.dev_guide.post.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.kuberix.dev_guide.common.exception.NotFoundException;
import com.kuberix.dev_guide.common.util.MessageUtil;
import com.kuberix.dev_guide.post.domain.Post;
import com.kuberix.dev_guide.post.domain.PostWrapper;
import com.kuberix.dev_guide.post.service.PostService;

@RestController
public class PostController {
	@Autowired
	private MessageUtil messageUtil;
	@Autowired
	private PostService postService;

	@PostMapping(value = "/v1/posts")
	@ResponseStatus(HttpStatus.CREATED)
	public void createPost(@Validated(Post.CreatingValidation.class) @RequestBody PostWrapper postWrapper) {
		postService.createPost(postWrapper.getPost());
	}

	@GetMapping(value = "/v1/posts")
	@ResponseStatus(HttpStatus.OK)
	public PostWrapper listPosts(@RequestParam(required = false) String title) {
		List<Post> posts = postService.listPosts(title);

		PostWrapper postWrapper = new PostWrapper();
		postWrapper.setPosts(posts);
		return postWrapper;
	}

	@GetMapping(value = "/v1/posts/{postId}")
	@ResponseStatus(HttpStatus.OK)
	public PostWrapper readPost(@PathVariable Integer postId) {
		Post post = postService.readPost(postId);
		if (post == null) {
			throw new NotFoundException(messageUtil.getErrorMessage("BRD00001", postId));
		}

		PostWrapper postWrapper = new PostWrapper();
		postWrapper.setPost(post);
		return postWrapper;
	}

	@PutMapping(value = "/v1/posts/{postId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updatePost(@PathVariable Integer postId,
			@Validated(Post.UpdatingValidation.class) @RequestBody PostWrapper postWrapper) {
		Post post = postService.readPost(postId);
		if (post == null) {
			throw new NotFoundException(messageUtil.getErrorMessage("BRD00002", postId));
		}

		postService.updatePost(postId, postWrapper.getPost());
	}

	@DeleteMapping(value = "/v1/posts/{postId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePost(@PathVariable Integer postId) {
		Post post = postService.readPost(postId);
		if (post == null) {
			throw new NotFoundException(messageUtil.getErrorMessage("BRD00003", postId));
		}

		postService.deletePost(postId);
	}
}
