package com.kuberix.dev_guide.post.mapper;

import java.util.List;

import com.kuberix.dev_guide.post.domain.Post;

public interface PostMapper {
	int createPost(Post post);

	Integer readCreatedPostId();

	List<Post> listPosts(Post post);

	Post readPost(Post post);

	int updatePost(Post post);

	int deletePost(Post post);
}
