package com.kuberix.dev_guide.post.domain;

import org.hibernate.validator.constraints.NotBlank;

public class Post {
	public interface CreatingValidation {
	}

	public interface UpdatingValidation {
	}

	private Integer postId;
	@NotBlank(message = "{not.blank} title", groups = { CreatingValidation.class, UpdatingValidation.class })
	private String title;
	@NotBlank(message = "{not.blank} content", groups = { CreatingValidation.class, UpdatingValidation.class })
	private String content;
	private String createdDateTime;
	private String updatedDateTime;

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

}
