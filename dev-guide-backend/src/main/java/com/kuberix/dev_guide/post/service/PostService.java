package com.kuberix.dev_guide.post.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuberix.dev_guide.post.domain.Post;
import com.kuberix.dev_guide.post.mapper.PostMapper;

@Service
public class PostService {
	@Autowired
	private PostMapper postMapper;

	public Integer createPost(Post post) {
		postMapper.createPost(post);
		return postMapper.readCreatedPostId();
	}

	public List<Post> listPosts(String title) {
		Post post = new Post();
		post.setTitle(title);
		return postMapper.listPosts(post);
	}

	public Post readPost(Integer postId) {
		Post post = new Post();
		post.setPostId(postId);
		return postMapper.readPost(post);
	}

	public void updatePost(Integer postId, Post post) {
		post.setPostId(postId);
		postMapper.updatePost(post);
	}

	public void deletePost(Integer postId) {
		Post post = new Post();
		post.setPostId(postId);
		postMapper.deletePost(post);
	}
}
