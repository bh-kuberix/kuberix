package com.kuberix.dev_guide.post.domain;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.kuberix.dev_guide.post.domain.Post.CreatingValidation;
import com.kuberix.dev_guide.post.domain.Post.UpdatingValidation;

public class PostWrapper {
	@Valid
	@NotNull(message = "{not.null} post", groups = { CreatingValidation.class, UpdatingValidation.class })
	private Post post;
	private List<Post> posts;

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

}
