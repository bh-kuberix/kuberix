package com.kuberix.dev_guide.system.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.kuberix.dev_guide.post.domain.Post;

@RestController
public class SystemController {
	@GetMapping(value = "/")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void listPosts(Post post) {
	}
}
