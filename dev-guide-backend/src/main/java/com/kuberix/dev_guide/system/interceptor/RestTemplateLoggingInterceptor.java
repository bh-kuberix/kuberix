package com.kuberix.dev_guide.system.interceptor;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.FileCopyUtils;

public class RestTemplateLoggingInterceptor implements ClientHttpRequestInterceptor {

	private static final Logger LOGGER = LoggerFactory.getLogger("rest.call");

	private final Integer RESPONSE_LOGGING_LENGTH = 1024 * 128;

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		traceRequest(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		response = new BufferingClientHttpResponseWrapper(response);
		traceResponse(response);

		return response;
	}

	private void traceRequest(HttpRequest request, byte[] body) throws IOException {
		LOGGER.debug("[===========================REQUEST=============================================]");
		LOGGER.debug("[Method : {}, URI : {}]", request.getMethod(), request.getURI());
		LOGGER.debug("[Request Headers]");
		for (Entry<String, List<String>> entry : request.getHeaders().entrySet()) {
			LOGGER.debug("  -> {}: {}", entry.getKey(), entry.getValue());
		}
		LOGGER.debug("[Request Body : {}]", new String(body, "UTF-8"));
	}

	private void traceResponse(ClientHttpResponse response) throws IOException {
		String contentType = "";
		if (response.getHeaders() != null && response.getHeaders().getContentType() != null)
			contentType = response.getHeaders().getContentType().toString();

		StringBuilder inputStringBuilder = new StringBuilder();
		if (response.getBody() != null && !contentType.contains("image")) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));
			String line = bufferedReader.readLine();
			while (line != null) {
				inputStringBuilder.append(line);
				inputStringBuilder.append('\n');
				line = bufferedReader.readLine();
			}

		}
		LOGGER.debug("[============================RESPONSE==========================================");
		LOGGER.debug("[Status : {} {}]", response.getStatusCode(), response.getStatusText());
		LOGGER.debug("[Headers : {}   ]", response.getHeaders());
		LOGGER.debug("[Response Body : {}]",
				inputStringBuilder.substring(0, Math.min(inputStringBuilder.length(), RESPONSE_LOGGING_LENGTH)));
		LOGGER.debug("[=========================REST TEMPLATE END====================================]");

		inputStringBuilder.setLength(0);
		inputStringBuilder.trimToSize();
	}

	public class BufferingClientHttpResponseWrapper implements ClientHttpResponse {

		private final ClientHttpResponse response;

		private byte[] body;

		BufferingClientHttpResponseWrapper(ClientHttpResponse response) {
			this.response = response;
		}

		@Override
		public HttpStatus getStatusCode() throws IOException {
			return this.response.getStatusCode();
		}

		@Override
		public int getRawStatusCode() throws IOException {
			return this.response.getRawStatusCode();
		}

		@Override
		public String getStatusText() throws IOException {
			return this.response.getStatusText();
		}

		@Override
		public HttpHeaders getHeaders() {
			return this.response.getHeaders();
		}

		@Override
		public InputStream getBody() throws IOException {
			if (this.body == null) {
				if (this.response.getBody() != null) {
					this.body = FileCopyUtils.copyToByteArray(this.response.getBody());
				}
			}
			return (this.body == null) ? null : new ByteArrayInputStream(this.body);
		}

		@Override
		public void close() {
			this.response.close();
		}

	}

}
