package com.kuberix.dev_guide.system.interceptor;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RepeatableHttpServletRequestWrapper extends HttpServletRequestWrapper {
	private static final Logger LOGGER = LoggerFactory.getLogger("debug");
	private HttpServletRequest request;

	private byte[] cachedContent;

	public RepeatableHttpServletRequestWrapper(HttpServletRequest request) {
		super(request);
		this.request = request;
		try {
			parseRequest();
		} catch (IOException e) {
			LOGGER.info("Error : ", e);
		}
	}

	public byte[] getCachedContent() {
		return cachedContent.clone();
	}

	private void parseRequest() throws IOException {
		int length = request.getContentLength();
		if (length < 0 || cachedContent != null) {
			return;
		}
		byte[] body = new byte[length];
		int len = 0;
		int count = 0;
		InputStream input = request.getInputStream();
		while (count < body.length) {
			count += (len = input.read(body, count, body.length - count));
			if (len < 1) {
				throw new IOException("Cannot read more than " + count + (count == 1 ? " byte!" : " bytes!"));
			}
		}
		cachedContent = body;
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		parseRequest();
		return new ServletInputStreamWrapper(cachedContent);
	}

	private class ServletInputStreamWrapper extends ServletInputStream {
		private byte[] data;

		private int idx = 0;

		protected ServletInputStreamWrapper(byte[] data) {
			if (data == null) {
				data = new byte[0];
			}
			this.data = data;
		}

		@Override
		public boolean isFinished() {
			if (idx == data.length) {
				return true;
			}
			return false;
		}

		@Override
		public boolean isReady() {
			return true;
		}

		@Override
		public void setReadListener(ReadListener arg0) {
			throw new RuntimeException("Not implemented");
		}

		@Override
		public int read() throws IOException {
			if (idx == data.length) {
				return -1;
			}
			return data[idx++];
		}

	}

}
