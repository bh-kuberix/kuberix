package com.kuberix.dev_guide.system.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

import com.kuberix.dev_guide.system.interceptor.RepeatableHttpServletRequestWrapper;
import com.kuberix.dev_guide.system.interceptor.RepeatableHttpServletResponseWrapper;
import com.kuberix.dev_guide.system.interceptor.RepeatableHttpServletResponseWrapper.ServletOutputStreamWrapper;

public class RequestLoggingFilter extends AbstractRequestLoggingFilter {
	private static final Logger LOGGER = LoggerFactory.getLogger("received.api");

	private boolean includeHearder;

	private final Integer RESPONSE_LOGGING_LENGTH = 1024 * 128;

	public RequestLoggingFilter() {
		setIncludeQueryString(true);
		setIncludePayload(true);
		setIncludeHearder(true);
		setIncludeClientInfo(true);
	}

	public void setIncludeHearder(boolean includeHearder) {
		this.includeHearder = includeHearder;
	}

	@Override
	protected String createMessage(HttpServletRequest request, String prefix, String suffix) {
		final String requestURI = request.getRequestURI();

		String authToken = request.getHeader("Authorization");
		Map<String, Object> map = new HashMap<>();
		map.put("token", authToken);
		StringBuilder msg = new StringBuilder();
		msg.append(prefix);
		msg.append("method=").append(request.getMethod());
		msg.append(";uri=").append(requestURI);
		String queryString = request.getQueryString();
		if (isIncludeQueryString() && queryString != null) {
			msg.append('?').append(queryString);
			map.put("received_json_uri", request.getRequestURL().toString() + "?" + queryString);
		} else {
			map.put("received_json_uri", request.getRequestURL().toString());
		}
		if (isIncludeClientInfo()) {
			String client = request.getRemoteAddr();
			if (StringUtils.hasLength(client)) {
				msg.append(";client=").append(client);
			}
			HttpSession session = request.getSession(false);
			if (session != null) {
				msg.append(";session=").append(session.getId());
			}
			String user = request.getRemoteUser();
			if (user != null) {
				msg.append(";user=").append(user);
			}
		}
		if (includeHearder) {
			Enumeration<String> hearedNames = request.getHeaderNames();
			msg.append(";headers=[");
			int index = 0;
			while (hearedNames.hasMoreElements()) {
				String header = hearedNames.nextElement();
				if (index > 0) {
					msg.append(",{").append(header).append(": ").append(request.getHeader(header)).append("}");
				} else {
					msg.append("{").append(header).append(": ").append(request.getHeader(header)).append("}");
				}
				++index;
			}
			msg.append("]");
		}
		String payload = null;
		if (isIncludePayload() && request instanceof RepeatableHttpServletRequestWrapper) {
			RepeatableHttpServletRequestWrapper wrapper = (RepeatableHttpServletRequestWrapper) request;
			int length = wrapper.getContentLength();
			if (length > 0) {
				try {
					payload = IOUtils.toString(wrapper.getInputStream());
				} catch (IOException e) {
					payload = "[unknown]";
				}
				msg.append(";payload=").append(payload);
			}
		}
		map.put("received_json_message", payload);
		msg.append(suffix);
		final String logMessage = msg.toString();

		return logMessage;
	}

	protected String createResponseMessage(HttpServletResponse response, String prefix, String suffix) {
		StringBuilder msg = new StringBuilder();
		int status = response.getStatus();

		msg.append(prefix);
		msg.append("http-status={code=").append(status).append("}");
		if (isIncludePayload() && response instanceof RepeatableHttpServletResponseWrapper) {
			RepeatableHttpServletResponseWrapper wrapper = (RepeatableHttpServletResponseWrapper) response;
			try {
				wrapper.flushBuffer();

				byte[] re = ((ServletOutputStreamWrapper) wrapper.getOutputStream()).getCopy();
				String body = new String(re, response.getCharacterEncoding());
				msg.append(";body=").append(body.substring(0, Math.min(body.length(), RESPONSE_LOGGING_LENGTH)));
			} catch (IOException e) {
				msg.append(";error=").append("[exception]");
			}

		}
		msg.append(suffix);
		final String payload = msg.toString();
		msg.setLength(0);
		msg.trimToSize();
		return payload;
	}

	/**
	 * Forwards the request to the next filter in the chain and delegates down to
	 * the subclasses to perform the actual request logging both before and after
	 * the request is processed.
	 *
	 * @see #beforeRequest
	 * @see #afterRequest
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String type = response.getHeader("Content-Type");
		if (type != null && type.contains("multipart/form-data")) {
			super.doFilterInternal(request, response, filterChain);
		} else {
			boolean isFirstRequest = !isAsyncDispatch(request);
			HttpServletRequest requestToUse = request;
			if (isIncludePayload() && isFirstRequest && !(request instanceof RepeatableHttpServletRequestWrapper)) {
				requestToUse = new RepeatableHttpServletRequestWrapper(request);
			}

			HttpServletResponse responseToUse = response;
			if (isIncludePayload() && isFirstRequest && !(response instanceof RepeatableHttpServletResponseWrapper)) {
				responseToUse = new RepeatableHttpServletResponseWrapper(response);
			}

			if (isFirstRequest) {
				beforeRequest(requestToUse,
						createMessage(requestToUse, DEFAULT_BEFORE_MESSAGE_PREFIX, DEFAULT_BEFORE_MESSAGE_SUFFIX));
			}
			try {
				filterChain.doFilter(requestToUse, responseToUse);
			} finally {
				if (!isAsyncStarted(requestToUse)) {

					afterRequest(requestToUse, createResponseMessage(responseToUse, DEFAULT_AFTER_MESSAGE_PREFIX,
							DEFAULT_BEFORE_MESSAGE_SUFFIX));
				}
			}
		}

	}

	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
		LOGGER.info(message);
	}

	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
		LOGGER.info(message);
	}
}
