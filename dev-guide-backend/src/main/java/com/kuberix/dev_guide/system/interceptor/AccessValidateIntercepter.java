package com.kuberix.dev_guide.system.interceptor;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AccessValidateIntercepter extends HandlerInterceptorAdapter implements InitializingBean {
	private String exceptionURI[];

	public void setExceptionURI(String... urls) {
		exceptionURI = urls;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws IOException {
		String requestURL = request.getRequestURI();
		if (exceptionURI != null && exceptionURI.length > 0) {
			for (String url : exceptionURI) {
				Pattern p = Pattern.compile(url);
				Matcher m = p.matcher(requestURL);
				if (m.matches()) {
					return true;
				}
			}
		}

		// Validates
		// String authorization = request.getHeader("Authorization");

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterPropertiesSet() throws Exception {

	}

}
