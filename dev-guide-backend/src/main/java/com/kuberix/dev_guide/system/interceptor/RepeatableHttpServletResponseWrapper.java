package com.kuberix.dev_guide.system.interceptor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class RepeatableHttpServletResponseWrapper extends HttpServletResponseWrapper {

	private HttpServletResponse response;
	private ServletOutputStream outputStream;
	private ServletOutputStreamWrapper outputWrapper;
	private PrintWriter writer;

	public RepeatableHttpServletResponseWrapper(HttpServletResponse response) {
		super(response);
		this.response = response;
	}

	@Override
	public PrintWriter getWriter() throws IOException {
		if (writer == null) {
			writer = new CopyPrinterWriter(response.getWriter());
		}
		return writer;
	}

	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		if (outputStream == null) {
			outputStream = response.getOutputStream();
			outputWrapper = new ServletOutputStreamWrapper(response.getOutputStream());
		}
		return outputWrapper;
	}

	@Override
	public void flushBuffer() throws IOException {
		if (writer != null) {
			writer.flush();
		} else if (outputStream != null) {
			outputWrapper.flush();
		}
	}

	public class CopyPrinterWriter extends PrintWriter {
		private StringBuilder copy = new StringBuilder();

		public CopyPrinterWriter(Writer out) {
			super(out);
		}

		@Override
		public void write(int c) {
			copy.append((char) c);
			super.write(c);
		}

		@Override
		public void write(char[] buf, int off, int len) {
			copy.append(buf, off, len);
			super.write(buf, off, len);
		}

		@Override
		public void write(String s, int off, int len) {
			copy.append(s, off, len);
			super.write(s, off, len);
		}

		public String getCopy() {
			return copy.toString();
		}

	}

	public class ServletOutputStreamWrapper extends ServletOutputStream {
		private OutputStream outputStream;
		private ByteArrayOutputStream copy;

		public ServletOutputStreamWrapper(OutputStream out) {
			this.outputStream = out;
			this.copy = new ByteArrayOutputStream(1024);
		}

		@Override
		public void write(int b) throws IOException {
			outputStream.write(b);
			copy.write(b);
		}

		public byte[] getCopy() {
			return copy.toByteArray();
		}

		@Override
		public boolean isReady() {
			return true;
		}

		@Override
		public void setWriteListener(WriteListener arg0) {
			throw new RuntimeException("Not implemented");
		}

	}

}
