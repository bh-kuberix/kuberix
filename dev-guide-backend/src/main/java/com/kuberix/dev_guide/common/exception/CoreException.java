package com.kuberix.dev_guide.common.exception;

import org.springframework.http.HttpStatus;

public class CoreException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String code;
	private String message;
	private String[] details;
	private String title;
	private HttpStatus httpStatus;
	private Boolean isRefresh;

	public CoreException(HttpStatus httpStatus) {
		super();
		setAttributes(httpStatus.toString(), httpStatus.getReasonPhrase(), httpStatus.getReasonPhrase(), null,
				httpStatus);
	}

	public CoreException(HttpStatus httpStatus, String message) {
		super();
		setAttributes(httpStatus.toString(), httpStatus.getReasonPhrase(), message, null, httpStatus);
	}

	public CoreException(HttpStatus httpStatus, String message, String... details) {
		super();
		setAttributes(httpStatus.toString(), httpStatus.getReasonPhrase(), message, details, httpStatus);
	}

	public CoreException(HttpStatus httpStatus, Throwable cause) {
		super(cause);
		setAttributes(httpStatus.toString(), httpStatus.getReasonPhrase(), cause.getMessage(), null, httpStatus);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String[] getDetails() {
		return details;
	}

	public void setDetails(String[] details) {
		this.details = details;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public Boolean getIsRefresh() {
		return isRefresh;
	}

	public void setIsRefresh(Boolean isRefresh) {
		this.isRefresh = isRefresh;
	}

	private void setAttributes(String code, String title, String message, String[] details, HttpStatus httpStatus) {
		this.code = code;
		this.title = title;
		this.message = message;
		this.details = details;
		this.httpStatus = httpStatus;
	}

}
