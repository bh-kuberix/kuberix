package com.kuberix.dev_guide.common.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends CoreException {
	private static final long serialVersionUID = 1L;

	public NotFoundException() {
		super(HttpStatus.NOT_FOUND);
	}

	public NotFoundException(String message) {
		super(HttpStatus.NOT_FOUND, message);
	}

}
