package com.kuberix.dev_guide.common.domain;

public class ErrorMessageDetail {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
