package com.kuberix.dev_guide.common.domain;

import java.util.List;

/**
 * TODO add description.
 *
 * <p>
 * Class responsibility:
 * </p>
 */

public class ErrorMessage {

	private String code;

	private String message;

	private List<ErrorMessageDetail> details;

	private String title;
	private Boolean isRefresh;

	/**
	 * This method retrieves code.
	 *
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * This method sets code.
	 *
	 * @param code code to set.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * This method retrieves message.
	 *
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * This method sets message.
	 *
	 * @param message message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * This method retrieves details.
	 *
	 * @return details
	 */
	public List<ErrorMessageDetail> getDetails() {
		return details;
	}

	/**
	 * This method sets details.
	 *
	 * @param details details to set.
	 */
	public void setDetails(List<ErrorMessageDetail> details) {
		this.details = details;
	}

	/**
	 * This method retrieves title.
	 *
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * This method sets title.
	 *
	 * @param title title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getIsRefresh() {
		return isRefresh;
	}

	public void setIsRefresh(Boolean isRefresh) {
		this.isRefresh = isRefresh;
	}

}
