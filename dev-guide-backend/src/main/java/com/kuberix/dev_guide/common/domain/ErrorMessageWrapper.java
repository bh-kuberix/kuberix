package com.kuberix.dev_guide.common.domain;

public class ErrorMessageWrapper {

	private ErrorMessage errorMessage;

	public ErrorMessage getError() {
		return errorMessage;
	}

	public void setError(ErrorMessage errorMessage) {
		this.errorMessage = errorMessage;
	}

}
