package com.kuberix.dev_guide.common.exception;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

public class BadRequestException extends CoreException {
	private static final long serialVersionUID = 1L;

	public BadRequestException() {
		super(HttpStatus.BAD_REQUEST);
	}

	public BadRequestException(String message) {
		super(HttpStatus.BAD_REQUEST, message);
	}

	public BadRequestException(List<String> messageList) {
		super(HttpStatus.BAD_REQUEST, StringUtils.join(messageList, ' '));
	}

	public BadRequestException(String message, String[] details) {
		super(HttpStatus.BAD_REQUEST, message, details);
	}
}
