package com.kuberix.dev_guide.common.handler;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.kuberix.dev_guide.common.domain.ErrorMessage;
import com.kuberix.dev_guide.common.domain.ErrorMessageDetail;
import com.kuberix.dev_guide.common.domain.ErrorMessageWrapper;
import com.kuberix.dev_guide.common.exception.BadRequestException;
import com.kuberix.dev_guide.common.exception.CoreException;
import com.kuberix.dev_guide.common.exception.NotFoundException;

@RestControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger ERROR_LOG = LoggerFactory.getLogger("error");

	@ResponseBody
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorMessageWrapper handleException(HttpServletRequest request, HttpServletResponse response,
			Exception exception) {
		ERROR_LOG.error("Exception Handler", exception);

		ErrorMessageWrapper responseErrorBean = new ErrorMessageWrapper();
		ErrorMessage errorMessage = new ErrorMessage();

		errorMessage.setCode(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()));
		errorMessage.setTitle("Internal Server Error");

		String message = null;
		if (exception != null) {
			if (exception.getMessage() != null) {
				message = exception.getMessage();

			} else {
				message = exception.toString();

			}
		}

		if (message == null) {
			message = "Internal Server Error";
		} else {
			message = message.replaceAll("\\{", " ").replaceAll("\\}", " ").replaceAll("\\[", " ")
					.replaceAll("\\]", " ").replaceAll("\"", "");
		}

		errorMessage.setMessage(message);
		responseErrorBean.setError(errorMessage);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		return responseErrorBean;
	}

	@ResponseBody
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorMessageWrapper handleHttpMessageNotReadableException(HttpMessageNotReadableException exception) {
		ERROR_LOG.error("HttpMessageNotReadableException Handler", exception);

		ErrorMessageWrapper errorMessageWrapper = new ErrorMessageWrapper();
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setCode(Integer.toString(HttpStatus.BAD_REQUEST.value()));
		errorMessage.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
		errorMessage.setMessage(exception.getMessage());
		errorMessageWrapper.setError(errorMessage);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		return errorMessageWrapper;
	}

	@ResponseBody
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorMessageWrapper handleMethodArgumentNotValidException(HttpServletRequest request,
			HttpServletResponse response, MethodArgumentNotValidException exception) {
		BindingResult bindingResult = exception.getBindingResult();
		if (bindingResult.hasErrors()) {
			String message = "Invalid request parameters";
			String[] details = bindingResult.getAllErrors().stream().map(error -> error.getDefaultMessage())
					.toArray(String[]::new);

			BadRequestException nfvBadRequestException = new BadRequestException(message, details);
			return handleCoreException(request, response, nfvBadRequestException);
		}

		return handleException(request, response, exception);
	}

	@ResponseBody
	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ErrorMessageWrapper handleNotFoundException(HttpServletRequest request, HttpServletResponse response,
			NotFoundException exception) {
		return handleCoreException(request, response, exception);
	}

	@ResponseBody
	@ExceptionHandler(BadRequestException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorMessageWrapper handleNotFoundException(HttpServletRequest request, HttpServletResponse response,
			BadRequestException exception) {
		return handleCoreException(request, response, exception);
	}

	public ErrorMessageWrapper handleCoreException(HttpServletRequest request, HttpServletResponse response,
			CoreException exception) {
		ERROR_LOG.error("CoreException Handler", exception);

		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setCode(exception.getCode());
		errorMessage.setTitle(exception.getTitle());
		errorMessage.setMessage(exception.getMessage());
		errorMessage.setIsRefresh(exception.getIsRefresh());

		if (exception.getDetails() != null && exception.getDetails().length > 0) {
			List<ErrorMessageDetail> detailMessageList = new ArrayList<>();

			for (String detail : exception.getDetails()) {
				ErrorMessageDetail errorDetailBean = new ErrorMessageDetail();
				String message = detail;
				errorDetailBean.setMessage(message);
				detailMessageList.add(errorDetailBean);
			}

			errorMessage.setDetails(detailMessageList);
		}

		ErrorMessageWrapper errorMessageWrapper = new ErrorMessageWrapper();
		errorMessageWrapper.setError(errorMessage);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

		return errorMessageWrapper;
	}

	@ResponseBody
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ErrorMessageWrapper handleNoHandlerFoundException(HttpServletRequest request, HttpServletResponse response,
			NoHandlerFoundException exception) {

		ERROR_LOG.error("NoHandlerFoundException Handler", exception);

		ErrorMessageWrapper errorMessageWrapper = new ErrorMessageWrapper();
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setCode(Integer.toString(HttpStatus.NOT_FOUND.value()));
		errorMessage.setTitle(HttpStatus.NOT_FOUND.getReasonPhrase());
		errorMessage.setMessage(exception.getMessage());
		errorMessageWrapper.setError(errorMessage);

		return errorMessageWrapper;
	}

}