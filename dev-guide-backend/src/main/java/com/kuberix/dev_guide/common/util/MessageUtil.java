package com.kuberix.dev_guide.common.util;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class MessageUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageUtil.class);
	@Autowired
	private MessageSource messageSource;

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessage(String messageCode, Object... parameters) {
		if (messageCode == null || messageCode.length() != 8) {
			return "Unknown message code: " + messageCode;
		}

		if (parameters != null && parameters.length > 0) {
			for (int i = 0; i < parameters.length; i++) {
				Object param = parameters[i];
				if (param instanceof Exception) {
					String msg = ((Exception) param).getMessage();
					if (msg == null) {
						msg = ((Exception) param).toString();
					}

					if (msg != null) {
						parameters[i] = msg;
					}
				}
			}
		}

		String msg;
		try {
			msg = messageSource.getMessage(messageCode, parameters, Locale.getDefault());
		} catch (Exception e) {
			msg = null;
			LOGGER.error("get message error.", e);
		}

		if (msg == null) {
			return "Unknown message code: " + messageCode;
		} else {
			return msg;
		}
	}

	public String getErrorMessage(String messageCode, Object... parameters) {
		return "[" + messageCode + "]\n" + getMessage(messageCode, parameters);
	}
}
