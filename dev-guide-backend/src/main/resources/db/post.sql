create table post (
  postId int not null identity,
  title varchar(1000) not null,
  content varchar(10000),
  createdDateTime timestamp default current_timestamp not null,
  updatedDateTime timestamp default current_timestamp not null,
  primary key(postId)
);
insert into post (title, content) values ('title 1', 'content 1')
